// The root URL for the RESTful services
var rootURL = "http://104.236.97.197:8080/StreamFyServices-1.0/rest/songs";

var currentWine;

// Retrieve songs list when application starts 
//findAll();

$('#botonListar').click(function() {
	alert("Hola");
	findAll();
});

function findAll() {
	console.log('findAll');
	$.ajax({
		type: 'GET',
		url: rootURL,
		dataType: "json", // data type of response
		success: renderList
	});
}

function renderList(data) {
	// JAX-RS serializes an empty list as null, and a 'collection of one' as an object (not an 'array of one')
	var list = data == null ? [] : (data instanceof Array ? data : [data]);

	$('#sonList li').remove();
	$.each(list, function(index, song) {
		$('#sonList').append('<li><a href="#" data-identity="' + song.id + '">'+song.name+'</a></li>');
	});
}

function renderDetails(wine) {
	$('#SongId').val(wine.id);
	$('#name').val(wine.name);
	$('#artist').val(wine.grapes);
	$('#album').val(wine.country);
	$('#path').val(wine.region);
	$('#status').val(wine.year);
	$('#year').attr('src', 'pics/' + wine.picture);
}

// Helper function to serialize all the form fields into a JSON string
function formToJSON() {
	var wineId = $('#wineId').val();
	return JSON.stringify({
		"id": wineId == "" ? null : wineId, 
		"name": $('#name').val(), 
		"artist": $('#artist').val(),
		"album": $('#album').val(),
		"path": $('#path').val(),
		"status": $('#status').val(),
		"year":  $('#year').val()
		
		});
}