<%-- 
    Document   : index
    Created on : 07-ago-2016, 11:22:54
    Author     : joseluis
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>MusicBeat Home Page2</title>

    <!-- Css Files -->
    <link href="css/bootstrap.css" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">
    <link href="css/flaticon.css" rel="stylesheet">
    <link href="style.css" rel="stylesheet">
    <link href="css/color.css" rel="stylesheet">
    <link href="css/color-one.css" rel="stylesheet">
    <link href="css/color-two.css" rel="stylesheet">
    <link href="css/slick-slider.css" rel="stylesheet">
    <link href="css/prettyphoto.css" rel="stylesheet">
    <link href="css/jplayer.css" rel="stylesheet">
    <link href="css/responsive.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
	
    <!--// Main Wrapper \\-->
    <div class="wm-main-wrapper">
        
        <!--// Header \\-->
		<header id="wm-header" class="wm-header-two">
			
			<!--// Top Strip \\-->
			<div class="wm-topstrip">
				<div class="container">

				</div>
			</div>
			<!--// Top Strip \\-->

			<!--// Main Header \\-->
			<div class="wm-main-header">
				<div class="container">
					<div class="row">
						<div class="col-md-3"><a href="index.html" class="wm-logo"><img src="images/logo2.png" alt=""></a></div>
						<div class="col-md-9">
							<!--// Navigation \\-->
							<nav class="navbar navbar-default">
							    <div class="navbar-header">
							      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse-1" aria-expanded="false">
							        <span class="sr-only">Toggle navigation</span>
							        <span class="icon-bar"></span>
							        <span class="icon-bar"></span>
							        <span class="icon-bar"></span>
							      </button>
							    </div>
							    <div class="collapse navbar-collapse" id="navbar-collapse-1">
							      <ul class="nav navbar-nav">
							        <li class="active"><a href="#">Home</a>
		
							        </li>

							        <li><a href="login.jsp">Login</a>
							        </li>

						
							      </ul>
							    </div>
							</nav>
							<!--// Navigation \\-->
						</div>
					</div>
				</div>
			</div>
			<!--// Main Header \\-->

		</header>
		<!--// Header \\-->

		<!--// Main Banner \\-->
		<div class="wm-main-banner">

			<div class="wm-banner-two">
				<div class="wm-bannertwo-slide">
					<div class="wm-banner-caption">
						<span>Charles Jemmington  //</span>
						<div class="clearfix"></div>
						<h1>Play The Moments</h1>
						<a href="album-list.html" class="wm-banner-btn wm-bgcolor"><i class="flaticon-tool"></i> Get New Album</a>
					</div>
					<div class="wm-banner-thumb"><img src="extra-images/banne-view2-1.jpg" alt=""></div>
				</div>
				<div class="wm-bannertwo-slide">
					<div class="wm-banner-caption">
						<span>Those beetlejuices  //</span>
						<div class="clearfix"></div>
						<h1>the side scenes</h1>
						<a href="album-list.html" class="wm-banner-btn wm-bgcolor"><i class="flaticon-tool"></i> Get New Album</a>
					</div>
					<div class="wm-banner-thumb"><img src="extra-images/banne-view2-2.jpg" alt=""></div>
				</div>
				<div class="wm-bannertwo-slide">
					<div class="wm-banner-caption">
						<span>we’ll dance all night //</span>
						<div class="clearfix"></div>
						<h1>Crazy Dancing</h1>
						<a href="album-list.html" class="wm-banner-btn wm-bgcolor"><i class="flaticon-tool"></i> Get New Album</a>
					</div>
					<div class="wm-banner-thumb"><img src="extra-images/banne-view2-3.jpg" alt=""></div>
				</div>
			</div>

		</div>
		<!--// Main Banner \\-->

		<!--// Main Content \\-->
		<div class="wm-main-content">

			<!--// Main Section \\-->
			<div class="wm-main-section wm-recent-releases-full">
				<div class="container">
					<div class="row">
						<div class="col-md-12">
							<div class="wm-simple-facny-title"><h2>Recent Releases</h2></div>
							<div class="wm-album wm-recent-releases-album">
								<ul class="row">
									<li class="col-md-4">
										<figure><a href="album-single-post.html"><img src="extra-images/album-recent-1.jpg" alt=""></a>
											<figcaption>
												<div class="wm-album-caption">
													<h2><a href="album-single-post.html">Acoustic, Music, Soul</a></h2>
													<ul>
														<li><span>Available on:</span></li>
														<li><a href="https://www.amazon.com/" class="flaticon-icon-2406"></a></li>
														<li><a href="http://www.apple.com/" class="flaticon-technology-2"></a></li>
														<li><a href="https://soundcloud.com/" class="flaticon-shape"></a></li>
													</ul>
													<div class="clearfix"></div>
													<a href="album-single-post.html" class="wm-album-linkbtn wm-color"><i class="flaticon-tool"></i></a>
													<a href="album-single-post.html" class="wm-album-linkbtn wm-color"><i class="flaticon-link"></i></a>
												</div>
											</figcaption>
										</figure>
										<div class="wm-recent-releases-text">
											<time datetime="2008-02-14 20:00">Date: 26/01/2016</time>
											<h2><a href="album-single-post.html">Album: You Know Who You Are</a></h2>
										</div>
									</li>
									<li class="col-md-4">
										<figure><a href="album-single-post.html"><img src="extra-images/album-recent-2.jpg" alt=""></a>
											<figcaption>
												<div class="wm-album-caption">
													<h2><a href="album-single-post.html">Acoustic, Music, Soul</a></h2>
													<ul>
														<li><span>Available on:</span></li>
														<li><a href="https://www.amazon.com/" class="flaticon-icon-2406"></a></li>
														<li><a href="http://www.apple.com/" class="flaticon-technology-2"></a></li>
														<li><a href="https://soundcloud.com/" class="flaticon-shape"></a></li>
													</ul>
													<div class="clearfix"></div>
													<a href="album-single-post.html" class="wm-album-linkbtn wm-color"><i class="flaticon-tool"></i></a>
													<a href="album-single-post.html" class="wm-album-linkbtn wm-color"><i class="flaticon-link"></i></a>
												</div>
											</figcaption>
										</figure>
										<div class="wm-recent-releases-text">
											<time datetime="2008-02-14 20:00">Date: 12/02/2016</time>
											<h2><a href="album-single-post.html">Album: Long Way Home</a></h2>
										</div>
									</li>
									<li class="col-md-4">
										<figure><a href="album-single-post.html"><img src="extra-images/album-recent-3.jpg" alt=""></a>
											<figcaption>
												<div class="wm-album-caption">
													<h2><a href="album-single-post.html">Acoustic, Music, Soul</a></h2>
													<ul>
														<li><span>Available on:</span></li>
														<li><a href="https://www.amazon.com/" class="flaticon-icon-2406"></a></li>
														<li><a href="http://www.apple.com/" class="flaticon-technology-2"></a></li>
														<li><a href="https://soundcloud.com/" class="flaticon-shape"></a></li>
													</ul>
													<div class="clearfix"></div>
													<a href="album-single-post.html" class="wm-album-linkbtn wm-color"><i class="flaticon-tool"></i></a>
													<a href="album-single-post.html" class="wm-album-linkbtn wm-color"><i class="flaticon-link"></i></a>
												</div>
											</figcaption>
										</figure>
										<div class="wm-recent-releases-text">
											<time datetime="2008-02-14 20:00">Date: 16/03/2016</time>
											<h2><a href="album-single-post.html">Album: You're Doomed. Be Nice.</a></h2>
										</div>
									</li>
								</ul>
							</div>
							<div class="wm-seeall-btn"><a href="album-list.html" class="wm-bgcolor">See All</a></div>
						</div>
					</div>
				</div>
			</div>
			<!--// Main Section \\-->





		</div>
		<!--// Main Content \\-->

		<!--// Footer \\-->
		<footer id="wm-footer" class="footer-two">





		</footer>
		<!--// Footer \\-->

	<div class="clearfix"></div>
    </div>
    <!--// Main Wrapper \\-->

	<!-- jQuery (necessary for JavaScript plugins) -->
	<script type="text/javascript" src="script/jquery.js"></script>
	<script type="text/javascript" src="script/modernizr.js"></script>
	<script type="text/javascript" src="script/jquery-ui.js"></script>
    <script type="text/javascript" src="script/bootstrap.min.js"></script>
    <script type="text/javascript" src="script/jquery.prettyphoto.js"></script>
    <script type="text/javascript" src="script/jquery.countdown.min.js"></script>
    <script type="text/javascript" src="script/fitvideo.js"></script>
    <script type="text/javascript" src="script/skills.js"></script>
    <script type="text/javascript" src="script/slick.slider.min.js"></script>
    <script type="text/javascript" src="script/jquery.jplayer.js"></script>
    <script type="text/javascript" src="script/jplayer.playlist.js"></script>
    <script type="text/javascript" src="script/jquery.nicescroll.min.js"></script>
    <script type="text/javascript" src="script/moment.min.js"></script>
    <script type="text/javascript" src="script/fullcalendar.min.js"></script>
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?sensor=false"></script>
    <script type="text/javascript" src="script/waypoints-min.js"></script>
    <script type="text/javascript" src="script/isotope.min.js"></script>
    <script type="text/javascript" src="script/functions.js"></script>
    <script type="text/javascript" src="script/jplayer.functions.js"></script>

  </body>
</html>
