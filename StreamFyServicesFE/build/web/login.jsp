<%-- 
    Document   : login
    Created on : 07-ago-2016, 7:19:52
    Author     : joseluis
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Login</title>

    <!-- Css Files -->
    <link href="css/bootstrap.css" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">
    <link href="css/flaticon.css" rel="stylesheet">
    <link href="style.css" rel="stylesheet">
    <link href="css/color.css" rel="stylesheet">
    <link href="css/color-one.css" rel="stylesheet">
    <link href="css/color-two.css" rel="stylesheet">
    <link href="css/color-three.css" rel="stylesheet">
    <link href="css/slick-slider.css" rel="stylesheet">
    <link href="css/prettyphoto.css" rel="stylesheet">
    <link href="css/jplayer.css" rel="stylesheet">
    <link href="css/responsive.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
	
    <!--// Main Wrapper \\-->
    <div class="wm-main-wrapper">
        
        <!--// Header \\-->
		<header id="wm-header" class="wm-header-two">
			
			<!--// Top Strip \\-->
			
			<!--// Top Strip \\-->

			<!--// Main Header \\-->
			<div class="wm-main-header">
				<div class="container">
					<div class="row">
						<div class="col-md-3"><a href="index.jsp" class="wm-logo"><img src="images/logo2.png" alt=""></a></div>
						<div class="col-md-9">
							<!--// Navigation \\-->
							<nav class="navbar navbar-default">
							    <div class="navbar-header">
							      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse-1" aria-expanded="false">
							        <span class="sr-only">Toggle navigation</span>
							        <span class="icon-bar"></span>
							        <span class="icon-bar"></span>
							        <span class="icon-bar"></span>
							      </button>
							    </div>
							    <div class="collapse navbar-collapse" id="navbar-collapse-1">
							      <ul class="nav navbar-nav">
							        <li><a href="index.jsp">HOME</a>
							        
							        </li>
							        <li class="active"><a href="#">LOGIN</a>
							        	<ul class="wm-dropdown-menu">
							        		<li><a href="login.jsp">LOGIN</a></li>
							        		<li><a href="signup.jsp">SIGN UP</a></li>
							        	</ul>
						        	</li>
							      </ul>
							    </div>
							</nav>
							<!--// Navigation \\-->
						</div>
					</div>
				</div>
			</div>
			<!--// Main Header \\-->

		</header>
		<!--// Header \\-->

		<!--// Main Content \\-->
		<div class="wm-main-content">
			<!--// Main Section \\-->
			
			<div class="wm-main-section">	
			</div>
			<div class="wm-main-section wm-contactus-form-bg" style="padding: 5% 39%;">				
				<div class="container">
					<div class="row">
						<div class="col-md-3" >
							<div class="wm-contactus-title">
								<h2>LOGIN</h2>																				
							</div>
							<div class="wm-contact-us-form-section">
								<form action="SAutentificacion" method="POST">
									<ul>
										<li class="full">
                                                                                    <input name="txtUsername" type="text" onfocus="if(this.value =='Your username') { this.value = ''; }" onblur="if(this.value == '') { this.value ='Tu usuario'; }" value="Tu usuario">
										</li>
										<li class="full">
											<input name="txtPassword" type="text" onfocus="if(this.value =='Tu password') { this.value = ''; }" onblur="if(this.value == '') { this.value ='Tu contrasenña'; }" value="Tu contraseña">
											<a href="signup.html" style="padding-left: 60%;">Crear cuenta</a>
										</li>
										<li class="full">
											<input type="submit" value="INGRESAR" name="btnIngresar" style="background-color:#ffc400; color: #ffffff;"
											>
										</li>
									</ul>																											
								</form>								
							</div>
						</div>												
					</div>
				</div>	
			</div>
			<!--// Main Section \\-->
		</div>
		<!--// Main Content \\-->

		<!--// Footer \\-->
		
		<!--// Footer \\-->

    </div>
    <!--// Main Wrapper \\-->

	<!-- jQuery (necessary for JavaScript plugins) -->
	<script type="text/javascript" src="script/jquery.js"></script>
	<script type="text/javascript" src="script/modernizr.js"></script>
	<script type="text/javascript" src="script/jquery-ui.js"></script>
    <script type="text/javascript" src="script/bootstrap.min.js"></script>
    <script type="text/javascript" src="script/jquery.prettyphoto.js"></script>
    <script type="text/javascript" src="script/jquery.countdown.min.js"></script>
    <script type="text/javascript" src="script/fitvideo.js"></script>
    <script type="text/javascript" src="script/skills.js"></script>
    <script type="text/javascript" src="script/slick.slider.min.js"></script>
    <script type="text/javascript" src="script/jquery.jplayer.js"></script>
    <script type="text/javascript" src="script/jplayer.playlist.js"></script>
    <script type="text/javascript" src="script/jquery.nicescroll.min.js"></script>
    <script type="text/javascript" src="script/moment.min.js"></script>
    <script type="text/javascript" src="script/fullcalendar.min.js"></script>
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?sensor=false"></script>
    <script type="text/javascript" src="script/waypoints-min.js"></script>
    <script type="text/javascript" src="script/isotope.min.js"></script>
    <script type="text/javascript" src="script/functions.js"></script>
    <script type="text/javascript" src="script/jplayer.functions.js"></script>
    
  </body>
</html>
