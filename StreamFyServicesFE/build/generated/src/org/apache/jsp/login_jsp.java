package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;

public final class login_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html;charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("<!DOCTYPE html>\n");
      out.write("<html lang=\"en\">\n");
      out.write("  <head>\n");
      out.write("    <meta charset=\"utf-8\">\n");
      out.write("    <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">\n");
      out.write("    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">\n");
      out.write("    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->\n");
      out.write("    <title>Login</title>\n");
      out.write("\n");
      out.write("    <!-- Css Files -->\n");
      out.write("    <link href=\"css/bootstrap.css\" rel=\"stylesheet\">\n");
      out.write("    <link href=\"css/font-awesome.css\" rel=\"stylesheet\">\n");
      out.write("    <link href=\"css/flaticon.css\" rel=\"stylesheet\">\n");
      out.write("    <link href=\"style.css\" rel=\"stylesheet\">\n");
      out.write("    <link href=\"css/color.css\" rel=\"stylesheet\">\n");
      out.write("    <link href=\"css/color-one.css\" rel=\"stylesheet\">\n");
      out.write("    <link href=\"css/color-two.css\" rel=\"stylesheet\">\n");
      out.write("    <link href=\"css/color-three.css\" rel=\"stylesheet\">\n");
      out.write("    <link href=\"css/slick-slider.css\" rel=\"stylesheet\">\n");
      out.write("    <link href=\"css/prettyphoto.css\" rel=\"stylesheet\">\n");
      out.write("    <link href=\"css/jplayer.css\" rel=\"stylesheet\">\n");
      out.write("    <link href=\"css/responsive.css\" rel=\"stylesheet\">\n");
      out.write("\n");
      out.write("    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->\n");
      out.write("    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->\n");
      out.write("    <!--[if lt IE 9]>\n");
      out.write("      <script src=\"https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js\"></script>\n");
      out.write("      <script src=\"https://oss.maxcdn.com/respond/1.4.2/respond.min.js\"></script>\n");
      out.write("    <![endif]-->\n");
      out.write("  </head>\n");
      out.write("  <body>\n");
      out.write("\t\n");
      out.write("    <!--// Main Wrapper \\\\-->\n");
      out.write("    <div class=\"wm-main-wrapper\">\n");
      out.write("        \n");
      out.write("        <!--// Header \\\\-->\n");
      out.write("\t\t<header id=\"wm-header\" class=\"wm-header-two\">\n");
      out.write("\t\t\t\n");
      out.write("\t\t\t<!--// Top Strip \\\\-->\n");
      out.write("\t\t\t\n");
      out.write("\t\t\t<!--// Top Strip \\\\-->\n");
      out.write("\n");
      out.write("\t\t\t<!--// Main Header \\\\-->\n");
      out.write("\t\t\t<div class=\"wm-main-header\">\n");
      out.write("\t\t\t\t<div class=\"container\">\n");
      out.write("\t\t\t\t\t<div class=\"row\">\n");
      out.write("\t\t\t\t\t\t<div class=\"col-md-3\"><a href=\"index.jsp\" class=\"wm-logo\"><img src=\"images/logo2.png\" alt=\"\"></a></div>\n");
      out.write("\t\t\t\t\t\t<div class=\"col-md-9\">\n");
      out.write("\t\t\t\t\t\t\t<!--// Navigation \\\\-->\n");
      out.write("\t\t\t\t\t\t\t<nav class=\"navbar navbar-default\">\n");
      out.write("\t\t\t\t\t\t\t    <div class=\"navbar-header\">\n");
      out.write("\t\t\t\t\t\t\t      <button type=\"button\" class=\"navbar-toggle collapsed\" data-toggle=\"collapse\" data-target=\"#navbar-collapse-1\" aria-expanded=\"false\">\n");
      out.write("\t\t\t\t\t\t\t        <span class=\"sr-only\">Toggle navigation</span>\n");
      out.write("\t\t\t\t\t\t\t        <span class=\"icon-bar\"></span>\n");
      out.write("\t\t\t\t\t\t\t        <span class=\"icon-bar\"></span>\n");
      out.write("\t\t\t\t\t\t\t        <span class=\"icon-bar\"></span>\n");
      out.write("\t\t\t\t\t\t\t      </button>\n");
      out.write("\t\t\t\t\t\t\t    </div>\n");
      out.write("\t\t\t\t\t\t\t    <div class=\"collapse navbar-collapse\" id=\"navbar-collapse-1\">\n");
      out.write("\t\t\t\t\t\t\t      <ul class=\"nav navbar-nav\">\n");
      out.write("\t\t\t\t\t\t\t        <li><a href=\"index.jsp\">HOME</a>\n");
      out.write("\t\t\t\t\t\t\t        \n");
      out.write("\t\t\t\t\t\t\t        </li>\n");
      out.write("\t\t\t\t\t\t\t        <li class=\"active\"><a href=\"#\">LOGIN</a>\n");
      out.write("\t\t\t\t\t\t\t        \t<ul class=\"wm-dropdown-menu\">\n");
      out.write("\t\t\t\t\t\t\t        \t\t<li><a href=\"login.jsp\">LOGIN</a></li>\n");
      out.write("\t\t\t\t\t\t\t        \t\t<li><a href=\"signup.jsp\">SIGN UP</a></li>\n");
      out.write("\t\t\t\t\t\t\t        \t</ul>\n");
      out.write("\t\t\t\t\t\t        \t</li>\n");
      out.write("\t\t\t\t\t\t\t      </ul>\n");
      out.write("\t\t\t\t\t\t\t    </div>\n");
      out.write("\t\t\t\t\t\t\t</nav>\n");
      out.write("\t\t\t\t\t\t\t<!--// Navigation \\\\-->\n");
      out.write("\t\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t</div>\n");
      out.write("\t\t\t</div>\n");
      out.write("\t\t\t<!--// Main Header \\\\-->\n");
      out.write("\n");
      out.write("\t\t</header>\n");
      out.write("\t\t<!--// Header \\\\-->\n");
      out.write("\n");
      out.write("\t\t<!--// Main Content \\\\-->\n");
      out.write("\t\t<div class=\"wm-main-content\">\n");
      out.write("\t\t\t<!--// Main Section \\\\-->\n");
      out.write("\t\t\t\n");
      out.write("\t\t\t<div class=\"wm-main-section\">\t\n");
      out.write("\t\t\t</div>\n");
      out.write("\t\t\t<div class=\"wm-main-section wm-contactus-form-bg\" style=\"padding: 5% 39%;\">\t\t\t\t\n");
      out.write("\t\t\t\t<div class=\"container\">\n");
      out.write("\t\t\t\t\t<div class=\"row\">\n");
      out.write("\t\t\t\t\t\t<div class=\"col-md-3\" >\n");
      out.write("\t\t\t\t\t\t\t<div class=\"wm-contactus-title\">\n");
      out.write("\t\t\t\t\t\t\t\t<h2>LOGIN</h2>\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\n");
      out.write("\t\t\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t\t\t<div class=\"wm-contact-us-form-section\">\n");
      out.write("\t\t\t\t\t\t\t\t<form action=\"SAutentificacion\" method=\"POST\">\n");
      out.write("\t\t\t\t\t\t\t\t\t<ul>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t<li class=\"full\">\n");
      out.write("                                                                                    <input name=\"txtUsername\" type=\"text\" onfocus=\"if(this.value =='Your username') { this.value = ''; }\" onblur=\"if(this.value == '') { this.value ='Tu usuario'; }\" value=\"Tu usuario\">\n");
      out.write("\t\t\t\t\t\t\t\t\t\t</li>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t<li class=\"full\">\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t<input name=\"txtPassword\" type=\"text\" onfocus=\"if(this.value =='Tu password') { this.value = ''; }\" onblur=\"if(this.value == '') { this.value ='Tu contrasenña'; }\" value=\"Tu contraseña\">\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t<a href=\"signup.html\" style=\"padding-left: 60%;\">Crear cuenta</a>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t</li>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t<li class=\"full\">\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t<input type=\"submit\" value=\"INGRESAR\" name=\"btnIngresar\" style=\"background-color:#ffc400; color: #ffffff;\"\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t</li>\n");
      out.write("\t\t\t\t\t\t\t\t\t</ul>\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\n");
      out.write("\t\t\t\t\t\t\t\t</form>\t\t\t\t\t\t\t\t\n");
      out.write("\t\t\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t\t</div>\t\t\t\t\t\t\t\t\t\t\t\t\n");
      out.write("\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t</div>\t\n");
      out.write("\t\t\t</div>\n");
      out.write("\t\t\t<!--// Main Section \\\\-->\n");
      out.write("\t\t</div>\n");
      out.write("\t\t<!--// Main Content \\\\-->\n");
      out.write("\n");
      out.write("\t\t<!--// Footer \\\\-->\n");
      out.write("\t\t\n");
      out.write("\t\t<!--// Footer \\\\-->\n");
      out.write("\n");
      out.write("    </div>\n");
      out.write("    <!--// Main Wrapper \\\\-->\n");
      out.write("\n");
      out.write("\t<!-- jQuery (necessary for JavaScript plugins) -->\n");
      out.write("\t<script type=\"text/javascript\" src=\"script/jquery.js\"></script>\n");
      out.write("\t<script type=\"text/javascript\" src=\"script/modernizr.js\"></script>\n");
      out.write("\t<script type=\"text/javascript\" src=\"script/jquery-ui.js\"></script>\n");
      out.write("    <script type=\"text/javascript\" src=\"script/bootstrap.min.js\"></script>\n");
      out.write("    <script type=\"text/javascript\" src=\"script/jquery.prettyphoto.js\"></script>\n");
      out.write("    <script type=\"text/javascript\" src=\"script/jquery.countdown.min.js\"></script>\n");
      out.write("    <script type=\"text/javascript\" src=\"script/fitvideo.js\"></script>\n");
      out.write("    <script type=\"text/javascript\" src=\"script/skills.js\"></script>\n");
      out.write("    <script type=\"text/javascript\" src=\"script/slick.slider.min.js\"></script>\n");
      out.write("    <script type=\"text/javascript\" src=\"script/jquery.jplayer.js\"></script>\n");
      out.write("    <script type=\"text/javascript\" src=\"script/jplayer.playlist.js\"></script>\n");
      out.write("    <script type=\"text/javascript\" src=\"script/jquery.nicescroll.min.js\"></script>\n");
      out.write("    <script type=\"text/javascript\" src=\"script/moment.min.js\"></script>\n");
      out.write("    <script type=\"text/javascript\" src=\"script/fullcalendar.min.js\"></script>\n");
      out.write("    <script type=\"text/javascript\" src=\"https://maps.googleapis.com/maps/api/js?sensor=false\"></script>\n");
      out.write("    <script type=\"text/javascript\" src=\"script/waypoints-min.js\"></script>\n");
      out.write("    <script type=\"text/javascript\" src=\"script/isotope.min.js\"></script>\n");
      out.write("    <script type=\"text/javascript\" src=\"script/functions.js\"></script>\n");
      out.write("    <script type=\"text/javascript\" src=\"script/jplayer.functions.js\"></script>\n");
      out.write("    \n");
      out.write("  </body>\n");
      out.write("</html>\n");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
